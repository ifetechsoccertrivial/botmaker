from __future__ import unicode_literals
import datetime
from django.db import models
from djangotoolbox.fields import ListField, EmbeddedModelField
from mongoengine import ObjectIdField


class Bot(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)
    description = models.TextField(blank=False)
    CATEGORY_CHOICES = (
        ('none', 'None'),
        ('health', 'Health'),
        ('tech', 'Technology'),
        ('fin_tech', 'Financial Technology'),
        ('business', 'Business'),
    )
    categories = models.CharField(max_length=8, choices=CATEGORY_CHOICES, default='none')
    created_on = models.DateTimeField(default=datetime.datetime.now())
    updated_on = models.DateTimeField(default=datetime.datetime.now())
    # messages = EmbeddedModelField('Messages')
    message_count = models.IntegerField(default=0)
    contexts = ListField()
    status = models.BooleanField(default=True)

    def __str__(self):
        return "{0} of {1}".format(self.name, self.categories)


class Users(models.Model):
    username = models.CharField(verbose_name="User Name", max_length=50)
    service_registered_for = ObjectIdField(verbose_name="Service ID")
    created_on = models.DateTimeField(verbose_name="Date of user creation")
    bot_id = ObjectIdField()


class Messages(models.Model):
    sender = models.CharField(verbose_name="User Name", max_length=50, default='')
    created_on = models.DateTimeField(verbose_name="Date of user creation", default=datetime.datetime.now())
    messages = ListField()
    bot_id = ObjectIdField()

