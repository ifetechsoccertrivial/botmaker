from django.conf.urls import url
from botmaker.views import BaseDashboardView, BotView, ProfileView, ChatView, BotListView


urlpatterns = [
    url(r'^$', BaseDashboardView.as_view(), name='login'),
    url(r'^bot/$', BotView.as_view(), name='bot'),
    url(r'^bot/profile/$', ProfileView.as_view(), name='profile'),
    url(r'^chat/$', ChatView.as_view(), name='main_chat'),
    url(r'^list/$', BotListView.as_view(), name='bot_list'),
    url(r'^logout/$', BotListView.as_view(), name='logout')
]