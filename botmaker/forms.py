from django import forms
from botmaker.models import Bot


class BotForm(forms.ModelForm):
    class Meta:
        model = Bot
        fields = ('name', 'description', 'categories')

    def __init__(self, *args, **kwargs):
        super(BotForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control form-control-line', 'required':True})
        self.fields['description'].widget.attrs.update({'class': 'form-control form-control-line','required':True})
        self.fields['categories'].widget.attrs.update({'class': 'form-control form-control-line', 'required':True})

