 $(document).ready(function () {
     "use strict";

     var getMessage = $('#msg_btn').on('click', function (event) {
         event.preventDefault();

         var _sent_msg = $('#message_box').val();
         $.toast({
             heading: 'Your message',
             text: _sent_msg,
             position: 'top-right',
             bgColor: '#9EC600',
             icon: 'warning',
             hideAfter: 3500,
             stack: 6
         });


 });

 getMessage();

 });
