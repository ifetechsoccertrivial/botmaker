from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer,ChatterBotCorpusTrainer,TwitterTrainer,UbuntuCorpusTrainer


class FreeTrainer(object):
    pass


class FileTrainer(object):
    pass


class BotFather:
    def __init__(self):
        self.chatterbot = ChatBot(
            name="Terrabot",
            storage_adapter="chatterbot.storage.MongoDatabaseAdapter",
            logic_adapters=[
                'chatterbot.logic.BestMatch'
            ],
            database='Terrabot'
        )

        self.trainer_classes = {
            "list": ListTrainer,
            "corpus": ChatterBotCorpusTrainer,
            "twitter": TwitterTrainer,
            "ubuntu": UbuntuCorpusTrainer,
            "facebook": "",
            "custom": ""
        }

    def initialize_bot(self):
        pass

    def train(self, data, training_type=None):
        if training_type == "list_trainer":
            self.chatterbot.set_trainer(ListTrainer)
            for key, values in data.iteritems:
                self.chatterbot.train(key, values)
                return self.handle_training_response(data)
        elif training_type == "free_trainer":
            self.chatterbot.set_trainer(FreeTrainer)
            self.chatterbot.train(data)
            return self.handle_training_response(data)
        elif training_type == "file_trainer":
            self.chatterbot.set_trainer(FileTrainer)
            file_type = self.decipher_extension(data['fileObj'])
            if file_type == "json":
                self.chatterbot.train(data['fileObj'])
            else:
                return self.handle_training_response(data)

    def handle_training_response(self, data):
        pass

    def get_response(self):
        pass

    def decipher_extension(self, param):
        return False
