from django.views.generic.list import ListView
from django.views.generic import DetailView
from django.views.generic.base import View
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from botmaker.bot_father.bot_father import BotFather
from .forms import BotForm
from django.http import HttpResponse
import utils

bot_util = utils.BotUtils()


class BaseDashboardView(TemplateView):
    template_name = "botmaker/index.html"

    def get(self, request, *args, **kwargs):
        bot_count, user_count, all_users = utils.get_all_bot_count(), utils.get_user_count(), utils.get_all_users()
        return render(request, self.template_name,  {'bot_count': bot_count, 'user_count': user_count, 'all_users':all_users})


class BotView(View):
    def __init__(self, **kwargs):
        super(BotView, self).__init__(**kwargs)
        self.form = BotForm()

    def get(self, request, action=None, *args, **kwargs):
        action = self.request.GET.get('action', action)
        if action == "create":
            errors = kwargs.get('errors', None)
            return render(request, 'botmaker/create_bot.html', {'form': self.form, 'errors': errors})

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            bot_form = BotForm(request.POST)
            if bot_form.is_valid():
                bot = bot_form.save()
                request.session['temp_data'] = bot
                return redirect('/bot/profile', bot_id=bot.id)
            else:
                errors = bot_form.errors
                return self.get(request, action="create", errors=errors)


class ProfileView(DetailView):
    template_name = "botmaker/bot_profile.html"

    def get(self, request, *args, **kwargs):
        action = self.request.GET.get('bot', None)
        if not action:
            bot_profile = request.session.get('temp_data')
            bot_user, messages = utils.get_user_for_service(bot_profile.id), \
                                 utils.get_last_five_messages(bot_profile.id)
            return render(request, self.template_name,
                          {'bot_profile': bot_profile, 'bot_user': bot_user, 'messages': messages})
        else:
            bot_profile = bot_util.get_one_from_mongo_id(action)
            bot_user, messages = utils.get_user_for_service(bot_profile.id), utils.get_last_five_messages(
                bot_profile.id)
            return render(request, self.template_name,
                          {'bot_profile': bot_profile, 'bot_user': bot_user, 'messages': messages})


class ChatView(DetailView):
    template_name = "botmaker/chat_.html"

    def get(self, request, *args, **kwargs):
        bot_father = BotFather()
        bot_father.initialize_bot()
        return render(request, self.template_name)


class BotListView(ListView):
    def __init__(self, **kwargs):
        super(BotListView, self).__init__(**kwargs)
        self.form = BotForm()
        self.template_name = 'botmaker/hurdle.html'

    def get(self, request, *args, **kwargs):
        bot_list = bot_util.get_all_bot()
        return render(request, self.template_name, {'bot_list': bot_list})


def logout(request):
    request.session.pop('temp_data')
    return redirect('auth')