from bson import ObjectId
from mongoengine.errors import DoesNotExist
from models import Users, Messages, Bot


class BotUtils:
    def __init__(self, bot_id=None):
        self.botdb = Bot
        self.id = None
        self.name = None
        self.contexts = None
        self.categories = None
        self.created_on = None
        self.description = None
        self.message_count = None
        self.messages = None

    def get_one_from_mongo_id(self, bot_id):
        try:
            bot_id = ObjectId(bot_id)
            bot = self.botdb.objects.get(id=bot_id)
            if bot:
                self.id = bot.id
                self.name = bot.name
                self.categories = bot.categories
                self.description = bot.description
                self.message_count = bot.message_count
                self.created_on = bot.created_on

                return self
            else:
                return None
        except DoesNotExist:
            return None

    def get_all_bot(self):
            bot = Bot.objects.all()
            return bot
            # self.messages = bot.messages

    # def is_first_message(self, fb_id=None):
    #     fb_id = self.fb_id if not fb_id else fb_id
    #     try:
    #         user = self.botdb.objects.get(fb_id=fb_id, has_sent_first_message=False)
    #         if user:
    #             return True
    #     except DoesNotExist:
    #         return False
    #
    # def update_last_seen(self, user):
    #     # now = datetime.datetime.now()
    #     timestamp = datetime.datetime.now()
    #     self.botdb.update({"user_id": user.id}, {"$set": {"last_seen": timestamp}})
    #
    # def update_has_first_message(self, fb_id=None):
    #     return True if self.botdb.objects.get(fb_id=fb_id).update(has_sent_first_message=True) else False
    #
    # def create_temp_user(self, recipient_id):
    #     pass


def get_user_for_service(bot_id):
    try:
        user = Users.objects.filter(id=bot_id)
        return user
    except DoesNotExist:
        return []


def get_user_count():
    return Users.objects.all().count


def handle_response(data=None):
        return {
            "status": "success",
            "service_id": data["service_id"],
            "response": {
                "training_details": "[{0} : {1}]".format(data['message'], data['response']),
                "message": "Trained bot... Learnt {} new things".format(len(data))
            }
        }


def get_last_five_messages(bot_id):
    return Messages.objects.filter(id=ObjectId(bot_id))\
        # .order_by('messages')


def get_all_bot_count():
    return Bot.objects.all().count


def get_all_users():
    return Users.objects.all()